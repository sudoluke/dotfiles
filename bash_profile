# .bash_profile
set -o vi

# detect git repo in shell

parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

export PS1="\u@\h: \[\033[34m\]\w \[\033[32m\]\$(parse_git_branch)\[\033[00m\] $ "
# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs
