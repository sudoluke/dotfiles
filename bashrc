if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH
## Not using this but maybe in the future I will
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

unset rc
# Format history file with timestamp
HISTTIMEFORMAT="%F %T "
# ignore anything added to the jrnl from history
HISTIGNORE="$HISTIGNORE:jrnl *"
HISTIGNORE="$HISTIGNORE:j *"

OSVER=$(awk -F= '/^NAME/ {print $2}' /etc/os-release)

#Aliases
alias ll="ls -lhG"
alias ls="ls -FG"
alias la="ls -lAhFG"
alias c="clear -x"
alias j="jrnl"
alias jt="jrnl -today"
alias jnew="jrnl < ~/Documents/jrnl/templates/journal.txt \
	&& jrnl -1 --edit"
alias jedit="jrnl -1 --edit"
alias t="todo.sh"
alias tls="todo.sh ls"
alias cpr="rsync -ah --info=progress2"
alias gcb="git checkout -b"
alias ga="git add"
alias gg="git log --decorate --graph"
alias gl="git log --name-only --decorate --graph"
alias pf="fzf --preview='batcat \
	--style=full \
	--color=always {}' \
	--bind shift-up:preview-page-up,shift-down:preview-page-down"
alias r="fc -s"
alias rebash="source ~/.bashrc"
alias status="sudo systemctl status"
alias bat="batcat"
alias fd="fdfind"

if [[ ${OSVER} == '"Pop!_OS"' ]]; then
  alias upgrade="sudo apt update && sudo apt upgrade -y"
  alias install="sudo apt install"
  alias search="apt search"
else
  alias upgrade="sudo dnf -y upgrade"
  alias install="sudo dnf -y install"
  alias search="dnf search"
fi
# Functions
##git push current branch
function gpb {
  git push $1 $(git branch --show-current)
}
## Check for blocked port at ip
### ex tz 192.168.1.2 80
function tz() {
  timeout 1s /bin/bash -c "2>/dev/null >/dev/tcp/$1/$2" \
	  && echo "connection to $1 port $2 succeeded" \
	  || echo "connection to $1 port $2 failed."
}
## Open a random manpage just for fun
function rtfm() {
  manpage=$(man -k . | awk '{print $1}' | sort -R | tail -1)
  man $manpage
  unset $manpage
}
# fuzzy finder
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
